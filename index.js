// Express JS Introduction

	// open source, unopinionated web application framework written in Javascript and hosted in Node.JS designed to simplify the web process

// Initial Step

	// Command
		// npm init -y
		// npm install express
		// touch
				// index. js
				// package.json
		// create file : .gitignore   >>  node_modules



// load expressjs module into our application and save it in a variable called express
const express = require('express')

// set local host port number
const port = 4000;

// create variable that uses and stores it as app
const app = express();

// middleware
// express.json() is a method which allow handling of streaming of data and automatically parse the incoming json from our request.
app.use(express.json());

// mock database
let users = [
		{
			username: "TStark300",
			email: "stark@gmail.com",
			password: "notPeterParker"
		},
		{
			username: "ThorThunder",
			email: "love@gmail.com",
			password: "iLoveStormBreaker"
		}
	]

// App Syntax:
	// app.method(<endpoint>,function for request and response)
	// CONTROLLER


//[HTTP method for GET]
	app.get("/", (request, response) =>

	{
		// response.status - writeHead
		// response.send - write and end
		response.status(201).send("Hello from express")
	})



	app.get("/greeting", (request, response) =>
	{
		response.status(201).send("Hello from Batch245-esmade")
	})

	app.get("/users", (request, response) => {
		response.send(users)
	})


//[HTTP method for POST]
	app.post("/users", (request, response) =>
	{
		let input = request.body;

		let newUser = {
			username: input.username,
			email: input.email,
			password: input.password
		}

		users.push(newUser)

		response.send(`The ${newUser.username} is now registered in our website with email ${newUser.email}!`);
	})


//[HTTP method for DELETE]
	app.delete("/users",(request, response) =>
	{
		let deleteUser = users.pop()
		response.send(deleteUser)
	
	})




//[HTTP method for PUT]
	app.put("/users/:index",(request,response)=>
	{	//wildcard
		let indexToBeUpdated = request.params.index

		console.log(typeof indexToBeUpdated);

		//to convert to number
		indexToBeUpdated = parseInt(indexToBeUpdated);

		if(indexToBeUpdated < users.length)
		{
			users[indexToBeUpdated].password = request.body.password
			response.send(users[indexToBeUpdated]);
		}
		else {
			response.status(404).send("Page not found. Error 404!")
		}

	})



	app.listen(port,()=>console.log("Server is running at port 4000"))
